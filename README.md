# cidenet-front

## Notas para la configuracion

Se debe configurar .env.development a la url de la API, por defecto el servidor arranca en este puerto
VUE_APP_BASE_URL=http://localhost:5004/api/

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
