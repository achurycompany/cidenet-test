import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    listEmployee: []
  },
  getters: {},
  mutations: {},
  actions: {
    // eslint-disable-next-line no-unused-vars
    async employeeEdit({ dispatch }, dataForm) {
      try {
        return new Promise((resolve, reject) => {
          axios
            .put(`employee/edit`, dataForm, {})
            .then((res) => {
              resolve(res.data);
            })
            .catch((error) => {
              reject(error.response.status);
            });
        });
      } catch (error) {
        throw new Error(error.response.data);
      }
    },
    // eslint-disable-next-line no-unused-vars
    async employeeDelete({ dispatch }, dataForm) {
      try {
        return new Promise((resolve, reject) => {
          axios
            .put(`employee/delete`, dataForm, {})
            .then((res) => {
              resolve(res.data);
            })
            .catch((error) => {
              reject(error.response.status);
            });
        });
      } catch (error) {
        throw new Error(error.response.data);
      }
    },
    // eslint-disable-next-line no-unused-vars
    async employeeAdd({ dispatch }, dataForm) {
      try {
        return new Promise((resolve, reject) => {
          axios
            .post(`employee/add`, dataForm, {})
            .then((res) => {
              resolve(res.data);
            })
            .catch((error) => {
              reject(error.response.status);
            });
        });
      } catch (error) {
        throw new Error(error.response.data);
      }
    },
    // eslint-disable-next-line no-unused-vars
    async generateEmail({ dispatch }, dataForm) {
      try {
        return new Promise((resolve, reject) => {
          axios
            .post(`employee/generate`, dataForm, {})
            .then((res) => {
              resolve(res.data);
            })
            .catch((error) => {
              reject(error.response.status);
            });
        });
      } catch (error) {
        throw new Error(error.response.data);
      }
    }
  },
  modules: {}
});
